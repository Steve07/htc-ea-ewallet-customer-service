package com.htc.ea.ewallet.dto;

public class CustomerDto {

	private int id;
	private String name ;
	private String surname;
	private Integer phoneNumber;
	private String status;
	private String email;
	
	 public CustomerDto() {
		}

		public CustomerDto(int id, String name, String surname, Integer phoneNumber, String status, String email ) {
			this.id = id;
			this.name = name;
			this.surname = surname;
			this.phoneNumber = phoneNumber;
			this.status=status;
			this.email=email;
			// TODO Auto-generated constructor stub
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public Integer getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(Integer phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("CustomerDto [id=").append(id)
					.append(", name=").append(name)
					.append(", surname=").append(surname)
					.append(", phoneNumber=").append(phoneNumber)
					.append(", status=").append(status)
					.append(", email=").append(email).append("]");
			return builder.toString();
		}
		
}
