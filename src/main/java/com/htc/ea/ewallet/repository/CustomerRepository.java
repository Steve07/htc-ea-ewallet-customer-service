package com.htc.ea.ewallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htc.ea.ewallet.model.dao.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>  {
	
}
