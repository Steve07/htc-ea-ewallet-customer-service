package com.htc.ea.ewallet.service;

import java.util.List;
import java.util.Optional;

import com.htc.ea.ewallet.model.dao.Customer;

public interface CustomerService {	
	
	boolean insert(Customer cus);
	List<Customer> loadAllInvoice();
	Optional <Customer> findInvoiceById(long customerid);
}
